<?php
namespace Leward\Radionomy\Configuration;

class RadionomyConfiguration
{
    
    private $apiKey;
    private $radioUid;
    
    private $currentSongUri = 'http://api.radionomy.com/currentsong.cfm';
    private $currentSongCachePath;
    
    /**
     * 
     * @param string $apiKey
     * @return \Leward\Radionomy\Configuration\RadionomyConfiguration
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
    
    /**
     * 
     * @param string $radioUid
     * @return \Leward\Radionomy\Configuration\RadionomyConfiguration
     */
    public function setRadioUid($radioUid)
    {
        $this->radioUid = $radioUid;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getRadioUid()
    {
        return $this->radioUid;
    }
    
    /**
     * 
     * @param string $currentSongUri
     * @return \Leward\Radionomy\Configuration\RadionomyConfiguration
     */
    public function setCurrentSongUri($currentSongUri)
    {
        $this->currentSongUri = $currentSongUri;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getCurrentSongUri()
    {
        return $this->currentSongUri;
    }
    
    /**
     * 
     * @param type $path
     * @return \Leward\Radionomy\Configuration\RadionomyConfiguration
     */
    public function setCurrentSongCachePath($path)
    {
        $this->currentSongCachePath = $path;
        $this->currentSongCallmeBackPath = $this->currentSongCachePath . '.callmeback';
        return $this;
    }
    
    public function getCurrentSongCachePath()
    {
        return $this->currentSongCachePath;
    }
    
}