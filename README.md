Changelog
---------------

**0.1**

  - First version

**0.2**

  - Catch RunTimeException which happens if there is no network
  - The lib can work with the current song API from http://radionomy.letoptop.fr/ 

