<?php
namespace Leward\Radionomy\Entity;

class Track 
{
    /**
     *
     * @var string
     */
    private $title;
    
    /**
     *
     * @var string
     */
    private $artists;
    
    /**
     *
     * @var string
     */
    private $cover;
    
    /**
     *
     * @var integer
     */
    private $duration;
    
    /**
     *
     * @var integer 
     */
    private $callMeBackTimestamp = 0;
    
    /**
     * 
     * @param string $title
     * @param string $artists
     * @param string $cover
     * @param string $duration
     */
    public function __construct($title = 'Unknown', $artists = 'Unknown', $cover = null, $duration = 0) 
    {
        $this->title = $title;
        $this->artists = $artists;
        $this->cover = $cover;
        $this->duration = (int) $duration;
    }
    
    /**
     * 
     * @param string $title
     * @return \Leward\Radionomy\Entity\Track
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * 
     * @param string $artists
     * @return \Leward\Radionomy\Entity\Track
     */
    public function setArtists($artists)
    {
        $this->artists = $artists;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getArtists()
    {
        return $this->artists;
    }
    
    /**
     * 
     * @param string $cover
     * @return \Leward\Radionomy\Entity\Track
     */
    public function setCover($cover)
    {
        $this->cover = $cover;
        return $this;
    }
    
    /**
     * 
     * @return string
     */
    public function getCover()
    {
        return $this->cover;
    }
    
    /**
     * 
     * @param integer $duration
     * @return \Leward\Radionomy\Entity\Track
     */
    public function setDuration($duration)
    {
        $this->duration = (int) $duration;
        return $this;
    }
    /**
     * 
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }
    
    /**
     * 
     * @param integer $callMeBackTimestamp
     * @return \Leward\Radionomy\Entity\Track
     */
    public function setCallMeBackTimestamp($callMeBackTimestamp)
    {
        $this->callMeBackTimestamp = (int) $callMeBackTimestamp;
        return $this;
    }
    
    /**
     * 
     * @return type
     */
    public function getCallMeBackTimestamp()
    {
        return $this->callMeBackTimestamp;
    }
    
    /**
     * Which properties of the object are serialized. 
     * @return array
     */
    public function __sleep() {
        return array(
            'title', 
            'artists', 
            'cover', 
            'duration', 
            'callMeBackTimestamp'
        );
    }
    
}