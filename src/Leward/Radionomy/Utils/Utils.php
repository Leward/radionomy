<?php
namespace Leward\Radionomy\Utils;

/**
 * Here are usefull functions used by Leward\Radionomy library. 
 */
class Utils
{
    /**
     * 
     * @param string $path
     * @return mixed Returns the unserialized content of the file or <b>FALSE</b> is something went wrong
     */
    public static function readFromCache($path)
    {
        if(!file_exists($path))
            return false;
        
        $cacheContent = file_get_contents($path);
        if($cacheContent === false)
            return false;

        $unserializedContent = unserialize($cacheContent);
        if($unserializedContent === false)
            return false;

        return $unserializedContent;
    }
    
    /**
    * 
    * @param string $path
    * @param mixed $data
    */
   public static function writeToCache($path, $data)
   {
       return file_put_contents($path, serialize($data));
   }
   
}
