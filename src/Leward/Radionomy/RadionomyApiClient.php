<?php
namespace Leward\Radionomy;

use Leward\Radionomy\Configuration\RadionomyConfiguration;
use Leward\Radionomy\Client\RadionomyHttpClient;
use Leward\Radionomy\Utils\Utils;
use Leward\Radionomy\Entity\Track;

/**
 * 
 */
class RadionomyApiClient
{
    /**
     *
     * @var type 
     */
    private $radionomyHttpClient;
    
    public function __construct(RadionomyConfiguration $radionomyConfig) 
    {
        $this->radionomyHttpClient = new RadionomyHttpClient($radionomyConfig);
    }
    
    /**
     * 
     * @return Track
     */
    public function getCurrentSong()
    {
        // Get the current song from cache if the cache is not obsolete
        $trackFromCache = $this->getCurrentSongFromCache();
        $isCacheObsolete = $trackFromCache->getCallMeBackTimestamp() < time();
        // echo "\n" . (($isCacheObsolete) ? 'Load from webservice' : 'Load from cache') ."\n";
        if(!$isCacheObsolete)
            return $this->getCurrentSongFromCache();
        
        // Get the current song from the api
        $xmlResult = $this->radionomyHttpClient->getCurrentSong();
        
        // Parse the result
        $currentTrack = null;
        if($xmlResult !== false)
        {
            // Init elements we want to fill in
            $trackXmlElement = $xmlResult->track;
            $currentTrack = new Track();
            $currentTrack->setTitle((string) $trackXmlElement->title);
            $currentTrack->setArtists((string) $trackXmlElement->artists);
            $currentTrack->setCover((string) $trackXmlElement->cover);
            $currentTrack->setDuration((string) $trackXmlElement->duration);
            $currentTrack->setCallMeBackTimestamp(time() + (((string) $trackXmlElement->callmeback) / 1000));
            
            // Cache the current track and the callmeback
            Utils::writeToCache($this->getConfig()->getCurrentSongCachePath(), $currentTrack);
        }
        
        return $currentTrack;
    }
    
    /**
     * 
     * @return Track
     */
    public function getCurrentSongFromCache()
    {
        $track = Utils::readFromCache($this->getConfig()->getCurrentSongCachePath());
        if(!$track || !($track instanceof Track))
            $track = new Track();
        return $track;
    }
    
    /**
     * 
     * @param string $xmlString
     * @return Track
     */
    protected function parseCurrentSongXml($xmlString)
    {
        $currentTrack = null;
        
        $xmlElement = new \SimpleXMLElement($xmlString);
        if($xmlElement === false)
            return null;
        
        // Analyze the XML
        $trackArray = $xmlElement->xpath('/tracks/track');
        if($trackArray !== false && count($trackArray) == 1)
        {
            $currentTrack->setCallMeBackTimestamp((intval($trackArray[0]->callmeback)) / 1000 + time());
            $currentTrack->setTitle($trackArray[0]->title);
            $currentTrack->setArtists($trackArray[0]->artists);
            $currentTrack->setCover($trackArray[0]->cover);
            $currentTrack->setDuration($trackArray[0]->duration);
        }
        
        return $currentTrack;
    }
    
    /**
     * 
     * @return RadionomyConfiguration
     */
    protected function getConfig()
    {
        return $this->radionomyHttpClient->getRadionomyConfig();
    }
}
