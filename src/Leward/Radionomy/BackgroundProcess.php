<?php
namespace Leward\Radionomy;

use Leward\Radionomy\Configuration\RadionomyConfiguration;
use Leward\Radionomy\RadionomyHttpClient;

/**
 * A Background process used to populate a cache and update it automatically. 
 */
class BackgroundProcess
{
    /**
     *
     * @var RadionomyApiClient
     */
    private $radionomyConfig;
    
    public function __construct(RadionomyConfiguration $radionomyConfig) 
    {
        $this->radionomyConfig = new RadionomyHttpClient($radionomyConfig);
    }
    
    public function run($callback = null)
    {
        
    }
    
    protected function defaultCallback() 
    {
        
    }
    
}
