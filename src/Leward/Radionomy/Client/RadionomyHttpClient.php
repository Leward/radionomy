<?php 
namespace Leward\Radionomy\Client;

use Buzz\Browser;
use Leward\Radionomy\Configuration\RadionomyConfiguration;

class RadionomyHttpClient
{
    /**
     * The Radionomy Configuration to for the the API calls
     * @var RadionomyConfiguration 
     */
    private $radionomyConfig;
    
    /**
     * The http client
     * @var Browser 
     */
    private $buzzBrowser;
    
    public function __construct(RadionomyConfiguration $radionomyConfig) 
    {
        $this->radionomyConfig = $radionomyConfig;
        $this->buzzBrowser = new Browser();
    }
    
    /**
     * 
     * @param RadionomyConfiguration $radionomyConfig
     * @return \Leward\Radionomy\RadionomyApiClient
     */
    public function setRadionomyConfig(RadionomyConfiguration $radionomyConfig)
    {
        $this->radionomyConfig = $radionomyConfig;
        return $this;
    }
    
    /**
     * 
     * @return RadionomyConfiguration
     */
    public function getRadionomyConfig()
    {
        return $this->radionomyConfig;
    }
    
    /**
     * 
     * @return \SimpleXMLElement;
     */
    public function getCurrentSong()
    {
        $uri = $this->radionomyConfig->getCurrentSongUri();
        $requestParams = array(
            'radiouid' => $this->radionomyConfig->getRadioUid(), 
            'apikey' => $this->radionomyConfig->getApiKey(), 
            'callmeback' => 'yes', 
            'type' => 'XML', 
            'cover' => 'yes', 
            'previous' => 'no'
        );
        $uri .= "?" . http_build_query($requestParams);
        
        try 
        {
            $response = $this->buzzBrowser->submit($uri, $requestParams);
        }
        catch(\RuntimeException $e)
        {
            return false;
        }
        
        $xmlResult = new \SimpleXMLElement($response->getContent());
        return $xmlResult;
    }
}